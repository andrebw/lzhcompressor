import java.io.*;
import java.nio.charset.StandardCharsets;

public class LempelZiv {

    public static final int bufferSize = 126;
    public static final int refSize = 4;

    public static String result = "";

    public LempelZiv() {
    }

    public static void main(String[] args) throws IOException {
        new LempelZiv().compress("size_test/diverse.lyx", "lz.txt");
        new LempelZiv().decompress("lz.txt", "size_test/out.txt");
    }

    public void compress(String fileIn, String fileOut) throws IOException {
        FileReader in = new FileReader(fileIn);
        DataOutputStream out = new DataOutputStream(new FileOutputStream(fileOut));

        StringBuffer buffer = new StringBuffer();

        StringBuilder match = new StringBuilder();
        StringBuilder nonMatch = new StringBuilder();
        int matchIndex = -1, offset = -1, rmBuffer = 0, temp = -1, cursor = 0;
        int matchBytes = 0, nonMatchBytes = 0;

        int read;
        char currentChar;
        while ((read = in.read()) != -1) {
            currentChar = (char) read;
            String t = result;
            temp = buffer.lastIndexOf(match.toString() + currentChar); // index of match

            // IF MATCH
            if (temp != -1) {
                matchIndex = temp + rmBuffer; // bufferIndex + # removed from buffer
                offset = cursor - matchIndex - match.length(); // the offset of index from current cursor
                match.append(currentChar);
                matchBytes += getNumBytesOfChar(currentChar);
            } // END OF MATCH
            // IF NOT MATCH
            else {
                // if previous match was valid - (FLUSH)
                if (matchBytes >= refSize) {
                    if (nonMatchBytes > 0)
                        nonMatchBytes = flushNonMatch(nonMatch,nonMatchBytes,out);
                    matchBytes = flushMatch(match,offset,out);
                }
                // if previous match was not valid - (add to nonMatch)
                else if (matchBytes > 0) {
                    nonMatch.append(match);
                    nonMatchBytes += matchBytes;

                    match.setLength(0);
                    matchBytes = 0;
                }

                nonMatch.append(currentChar);
                nonMatchBytes += getNumBytesOfChar(currentChar);
            }   // END OF NO MATCH

            // if nonMatch is too big - FLUSH
            if (nonMatchBytes + matchBytes + 3 >= bufferSize) {
                nonMatchBytes = flushNonMatch(nonMatch,nonMatchBytes,out);
            }

            // if match is too big - FLUSH
            if (match.length() >= bufferSize) {
                nonMatchBytes = flushNonMatch(nonMatch,nonMatchBytes,out);
                matchBytes = flushMatch(match,offset,out);
            }

            buffer.append(currentChar);
            rmBuffer += fixBuffer(buffer);
            cursor++;
        } // END OF LOOP

        // FLUSH REMAINING DATA
        // Invalid match - append to nonMatch:
        if (0 < matchBytes && matchBytes < refSize) {
            nonMatch.append(match);
            nonMatchBytes += matchBytes;
            match.setLength(0);
            matchBytes = 0;
        }
        // Flush nonMatch
        if (nonMatchBytes > 0) {
            flushNonMatch(nonMatch,nonMatchBytes,out);
        }
        // Flush if match
        if (matchBytes > 0) {
            flushMatch(match,offset,out);
        }

        out.flush(); out.close(); in.close();
    }

    public void decompress(String fileIn, String fileOut) throws IOException {
        DataInputStream in = new DataInputStream(new FileInputStream(fileIn));
        FileWriter out = new FileWriter(fileOut, StandardCharsets.UTF_8);

        StringBuilder result = new StringBuilder();

        while(in.available()>0) {
            int length = in.readByte();  // length of uncompressed sequence
            if (length < 0) {
                byte[] byteBuffer = new byte[Math.min(-length,in.available())];         // got an error, handled it B-)
                in.readFully(byteBuffer,0,Math.min(-length,in.available()));

                result.append(new String(byteBuffer));
            } else {
                int refLength = in.read();              // length of reference
                int index = result.length() - length;   // index of reference

                int init = result.length();

                if (index - init != 0) {
                    while (index + refLength >= init) {
                        result.append(result.substring(index,init));
                        refLength -= (init-index);
                    }
                }
                result.append(result.substring(index, index + refLength) );
            }
        }
        out.write(result.toString());
        out.flush(); out.close(); in.close();
    }

    private int flushMatch(StringBuilder match, int offset, DataOutputStream out) {
        try {
            out.write(offset);
            out.write(match.length());
            result += "["+offset+","+match.length()+"]";
            match.setLength(0); // empties match
        } catch (IOException e) {
            System.out.println("FLUSH MATCH");
            e.printStackTrace();
        }
        return 0;
    }

    private int flushNonMatch(StringBuilder nonMatch, int length, DataOutputStream out) {
        try {
            out.write(-length); // length plus extra bytes
            for (byte k : nonMatch.toString().getBytes()) out.write(k); // writes all bytes
            result += "["+length+"]"+nonMatch;
            nonMatch.setLength(0);
        } catch (IOException e) {
            System.out.println("FLUSH NON MATCH");
            e.printStackTrace();
        }
        return 0;
    }

    private int getNumBytesOfChar(char c) {
        String s = String.valueOf(c);
        return s.getBytes().length;
    }

    private int fixBuffer(StringBuffer buffer) {
        if (buffer.length() > bufferSize) {
            int remove = buffer.length()-bufferSize;
            buffer.delete(0,buffer.length()-bufferSize);
            return remove;
        }
        return 0;
    }
}
