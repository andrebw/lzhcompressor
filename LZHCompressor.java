import java.io.*;
import java.util.Scanner;

public class LZHCompressor {
    static String mode;
    static String inputFile ;
    static String outputFile;

    public static void main(String[] args) throws IOException {
        if (args.length == 2 || args.length == 4) {
            parseArgs(args);
        } else {
            if (args.length != 0) showUsage();
            getInput();
        }

        File output = null;

        try {
            if (mode.equals("c")) {
                output = compress();
            } else if (mode.equals("d")) {
                output = decompress();
            } else {
                System.err.println("Mode '" + mode + "' unknown");
                showUsage();
            }
        } catch (Exception e) {
            System.out.println("hmm? error?");
            showUsage();
        }

        if (output != null) {
            System.out.println("Output file:\n" + output.getAbsolutePath());
        }

//        inputFile = "diverse.txt";
//        outputFile = "size_test/c.lzh";
//        compress();
//
//        inputFile = "size_test/c.lzh";
//        outputFile = "size_test/out.txt";
//        decompress();
    }

    public static File compress() throws IOException {
        Huffman huffman = new Huffman();
        LempelZiv lempelZiv = new LempelZiv();

        File fileOut = new File(outputFile);
        lempelZiv.compress(inputFile,"temp.txt");
        huffman.compress("temp.txt",outputFile);

        return fileOut;
    }

    public static File decompress() throws IOException {
        Huffman huffman = new Huffman();
        LempelZiv lempelZiv = new LempelZiv();

        File fileOut = new File(outputFile);
        huffman.decompress(inputFile,"temp.txt");
        lempelZiv.decompress("temp.txt",outputFile);

        return fileOut;
    }

    public static void parseArgs(String[] args) {
        try {
            mode = String.valueOf(args[0].charAt(1));
            inputFile = args[1];
            outputFile = args[2];
        } catch (Exception e) { showUsage(); }
    }

    public static void showUsage() {
        String usageMsg = "Usage: java program <(-c|-d)> <input_file> <output_file>" +
                            "\n-c: compress" +
                            "\n-d: decompress";
        System.out.println(usageMsg);
        System.exit(-1);
    }

    public static void getInput() {
        Scanner sc = new Scanner(System.in);
        System.out.println("LZHCompressor v.1.0");
        System.out.print("\nCompress or decompress? (c/d): ");
        mode = sc.next();
        System.out.print("Input file location: ");
        inputFile = sc.next();
        System.out.print("Output file location: ");
        outputFile = sc.next();
        System.out.println();
        sc.close();
    }
}
