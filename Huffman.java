import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Huffman {

    private final int alphabetSize;
    private final int DEFAULT_ALPHABET_SIZE = 256; //Character.MAX_VALUE;

    public static void main(String[] args) throws IOException {
        new Huffman().compress("size_test/diverse.txt", "size_test/h.txt");
        new Huffman().decompress("size_test/h.txt", "size_test/out.txt");
    }

    public Huffman() {
        this.alphabetSize = DEFAULT_ALPHABET_SIZE;
    }

    public Huffman(int alphabetSize) {
        this.alphabetSize = alphabetSize;
    }

    public void compress(String inputFile, String outputFile) throws IOException {
        DataInputStream in = new DataInputStream(new FileInputStream(inputFile));
        DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));

        char[] data = new char[in.available()];
        for (int i = 0; i < data.length; i++) {
            data[i] = (char) (in.read());
        }

        String inputData = new String(data);

        int[] freq = createFrequencyTable(inputData);
        byte[] encoded = encode(inputData);

        // count # of different chars in the file
        int numCharacters = 0;
        for (int i = 0; i < alphabetSize; i++) if (freq[i] > 0) numCharacters++;
        out.writeChar(numCharacters); // write the number of characters to read later
        for (int i = 0; i < alphabetSize; i++) {
            if (freq[i] > 0) {
                out.writeChar(i);        // writes the freq table (index value)
                out.writeInt(freq[i]);
            }
        }
        out.write(encoded);
        out.flush();
        in.close(); out.close();
    }

    public void decompress(String inputFile, String outputFile) throws IOException {
        DataInputStream in = new DataInputStream(new FileInputStream(inputFile));
        DataOutputStream out = new DataOutputStream(new FileOutputStream(outputFile));

        int[] freq = new int[alphabetSize];
        int numEntries = in.readChar();        // number of different chars used
        for (int i = 0; i < numEntries; i++) {
            int index = in.readChar();
            int value = in.readInt();
            freq[index] = value;
        }

        byte[] bytes = new byte[in.available()];
        in.readFully(bytes,0,in.available());

        String decodedText = decode(bytes, freq, out);
        out.flush();
        out.close(); in.close();
    }

    public int[] getBitValues(String bits) {
        if (bits != null) {
            int[] b = new int[bits.length()];
            for (int i = 0; i < b.length; i++) {
                b[i] = Integer.parseInt(String.valueOf(bits.charAt(i)));
            }
            return b;
        }
        return null;
    }

    public byte[] encode(String data) {
        ArrayList<Byte> bytes = new ArrayList<>();
        int[] freq = createFrequencyTable(data);
        Node root = createHuffmanTree(freq);
        String[] encoding = createEncodingTable(root);
        int[][] bitEncoding = createBitEncoding(encoding);

        int numBits = 0;
        int byteIndex = -1;
        for (char c : data.toCharArray()) {
            int[] bits = bitEncoding[c];
            for (int i : bits) {
                if (numBits % 8 == 0) {
                    byteIndex++;
                    bytes.add((byte) 0);
                }
                byte b = bytes.get(byteIndex);
                if (i == 0) b = (byte) (b << 1);
                if (i == 1) b = ((byte) ((b << 1) + 1));
                bytes.set(byteIndex,b);
                numBits++;
            }
        }

        if (numBits%8 == 0) bytes.add((byte) 8);
        else bytes.add((byte) (numBits%8));

        byte[] bytesOut = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            bytesOut[i] = bytes.get(i);
        }
        return bytesOut;
    }


    public String decode(byte[] bytes, int[] freq, DataOutputStream out) throws IOException {
        Node root = createHuffmanTree(freq);
        Node node = root;
        int numBits = 8;
        for (int j = 0; j < bytes.length-1; j++) {
            byte b = bytes[j];
            if (j == bytes.length-2) {
                numBits = bytes[bytes.length-1];
                b = (byte) (b << (8-numBits));
            }
            for (int i = 0; i < numBits; i++) {
                if ((b>>(7-i)&0b1) == 0) node = node.left;
                else if ((b>>(7-i)&0b1) == 1) node = node.right;
                if (node.left == null && node.right == null) {
                    out.write((byte)node.character);
                    node = root;
                }
            }
        }
        return out.toString();
    }

    public String createBitString(String data, String[] encoding) {
        StringBuilder bitString = new StringBuilder();
        for (char c : data.toCharArray()) {
            bitString.append(encoding[c]);
        }
        return String.valueOf(bitString);
    }

    public int[][] createBitEncoding(String[] encodingTable) {
        int[][] bitEncoding = new int[encodingTable.length][];
        for (int i = 0; i < encodingTable.length; i++) {
            bitEncoding[i] = getBitValues(encodingTable[i]);
        }
        return bitEncoding;
    }

    public int[] createFrequencyTable(String data) {
        int[] freq = new int[alphabetSize];
        for(char c : data.toCharArray()) {
            freq[c]++;
        }
        return freq;
    }

    public Node createHuffmanTree(int[] freq) {
        PriorityQueue<Node> pq = new PriorityQueue<>(Comparator.comparingInt(n0 -> n0.frequency));
        for (int i = 0; i < freq.length; i++) {
            if (freq[i] > 0) {
                pq.add(new Node(i, freq[i]));
            }
        }
        if (pq.size() == 1){
            pq.add(new Node());
        }
        while (pq.size() > 1) {
            Node left = pq.poll();
            Node right = pq.poll();
            Node parent = new Node(left, right);
            pq.add(parent);
        }
        return pq.poll();
    }
    public String[] createEncodingTable(Node root) {
        String[] encodingTable = new String[alphabetSize];
        cetr(root,"", encodingTable);
        return encodingTable;
    }
    public void cetr(Node node, String s, String[] table) {
        if (node.left != null && node.right != null) {
            cetr(node.left, s + 0b0, table);
            cetr(node.right, s + 0b1, table);
        } else {
            table[node.character] = s;
        }
    }
    static class Node {
        int frequency;
        int character;
        Node left;
        Node right;

        public Node(int character, int frequency) {
            this.frequency = frequency;
            this.character = character;
        }
        public Node(Node left, Node right) {
            this.frequency = left.frequency + right.frequency;
            this.left = left;
            this.right = right;
        }
        public Node() {
            this.frequency = 0;
        }
        @Override
        public String toString() {
            return character + ": " + frequency;
        }
    }
}

