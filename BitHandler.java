
public class BitHandler {

    public static void printInt(int b) {
        for (int i = 0; i < 32; i++) {
            System.out.print( (b>>(31-i)) &0b1);
        }
        System.out.println();
    }

    public static void printByte(byte b) {
        for (int i = 0; i < 8; i++) {
            System.out.print( (b>>(7-i)) &0b1);
        }
        System.out.println();
    }

    public static void printChar(char c) {
        for (int i = 0; i < 16; i++) {
            System.out.print( (c>>(15-i)) &0b1);
        }
        System.out.println();
    }

    public static char[] byteToCharArray(byte[] byteArray) {
        char[] charArray = new char[byteArray.length];
        for (int i = 0; i < byteArray.length; i++) {
            charArray[i] = (char) byteArray[i];
        }
        return charArray;
    }
}
